# CSV To Json

This converts a json object stored in csv format to a json file

## Usage

### Installation
```
npm install scenes-csv-to-json --save-dev
```

### Scripts

Update package scripts for running scripts.
Add to package.json:
```
"csvToJson": "node node_modules/scenes-csv-to-json/src/csvToJson path/to/csvfile.csv path/to/output.json"
```

### Running

To create your desired json file:
```
npm run csvToJson
```
"use strict";

/**
 * @description Converts a csv file to JSON format
 * @use > node path/to/csvToJson inpath/name1.csv outpath/name2.json
 */

const fs = require("fs");
const csv = require("csvtojson");
const files = process.argv.slice(2);

const csvFilePath = `${files[0]}`;
const jsonFilePath = `${files[1]}`;

csv({
  colParser:{
      "id":"number",
      "business_logic_ex":"string"
  },
  checkType:true
})
.fromFile(csvFilePath)
.then((jsonObj) => {
    fs.writeFileSync(
      jsonFilePath,
      JSON.stringify(jsonObj, null, 2)
    );
    console.log(`Json created in ${jsonFilePath}`);
});